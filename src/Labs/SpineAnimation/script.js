import Assets from "../../Assets/assets.js";

const app = new PIXI.Application({ width: window.innerWidth, height: window.innerHeight, });
document.body.appendChild(app.view);

const appWidth = app.screen.width;
const appHeight = app.screen.height;

const container = new PIXI.Container();
app.stage.addChild(container);

const assets = {
	SpineBoyEss: Assets.spine.SpineBoy.json,
};

const assetsKeys = Object.keys(assets);
let key = assetsKeys[0];
let i = 0;

assetsKeys.forEach(key => {
	app.loader.add(key, assets[key]);
});

drawCharacter();
function drawCharacter() {
	app.loader.load((loader, resources) => {
		while (app.stage.children[0]) app.stage.removeChild(app.stage.children[0]); // Remove All
		const resource = resources[key];
		const animationsKeys = Object.keys(resource.data.animations);
		const animations = new PIXI.spine.Spine(resource.spineData);

		if (i === 1 && animationsKeys.length === 1) i = 0;
		if (animations.state.hasAnimation(animationsKeys[i])) {
			animations.state.setAnimation(0, animationsKeys[i], true);
		}
		app.stage.addChild(animations);

		const scaleAnim = 200 / animations.height;
		animations.scale.x = scaleAnim;
		animations.scale.y = scaleAnim;
		animations.position.set(appWidth / 2, appHeight / 3 * 2);
		app.start();
	});
}

const characterSelect = document.getElementById("Character");
characterSelect.addEventListener("change", () => {
	key = characterSelect.value;
	drawCharacter();
});

const animationTypeSelect = document.getElementById("type");
animationTypeSelect.addEventListener("change", () => {
	i = Number(animationTypeSelect.value);
	drawCharacter();
});
