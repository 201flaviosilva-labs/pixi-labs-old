// type="module"
// import Assets from "../../../Assets/Assets.js";

export const assetsPath = "/src/Assets/";
export const phaserAssetsPath = "https://labs.phaser.io/assets/";

const Assets = {
	image: {
		Russia: {
			Original: assetsPath + "Image/Russia/Original.png",
			Putin: assetsPath + "Image/Russia/Putin.png",
		}
	},
	spine: {
		SpineBoy: {
			atlas: assetsPath + "Spine/SpineBoy/spineboy-ess.atlas",
			json: assetsPath + "Spine/SpineBoy/spineboy-ess.json",
			png: assetsPath + "Spine/SpineBoy/spineboy-ess.png",
		}
	}
};

export default Assets;
